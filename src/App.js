import React, {useState, useEffect} from 'react';
import InputGroup from "./components/InputGroup/InputGroup"
import Item from "./components/Item/Item"
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3)
    }
}))

function App() {
    const classes = useStyles()

    const [person, setPerson] = useState({});
    const [items, setItems] = useState([]);
    const url = 'http://146.185.154.90:8000/messages';

    const printValues = event => {
        const name = event.target.name;
        const value = event.target.value;
        const copyPerson = {...person};
        copyPerson[name] = value;
        setPerson(copyPerson)
    }

    const sendMassage = async () => {
        const data = new URLSearchParams();
        data.set('author', person.author);
        data.set('message', person.message);
        const response = await fetch(url, {
            method: 'post',
            body: data,
        });
        if (response.ok) {
            await response.json()
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    async function queryAPI(addStr = "") {
        const response = await fetch(url + addStr);
        if (response.ok) {
            const json = await response.json();
            setItems(json);
        } else {
            alert("Ошибка HTTP: " + response.status);
        }
    }

    let item = items.map(item => {
        return <Item
            fullWidth
            key={item.id}
            id={item.id}
            author={item.author}
            message={item.message}
            date={item.datetime}
        >
        </Item>
    })

    useEffect(() => {
        queryAPI();
    }, []);

    useEffect(() => {
        const interval = setInterval(() => {
            const getNewMessage = async () => {
                const index = [items.length - 1]
                let lastMessage = items[index];
                const lastdate = lastMessage.datetime;
                const response = await fetch(`${url}?datetime=${lastdate}`);
                if (response.ok) {
                    const json = await response.json();
                    if (json.length > 0) {
                        const copyItem = [...items]
                        json.forEach((item) => {
                            copyItem.push(item)
                        })
                        setItems(copyItem)
                    }
                }
            };
            getNewMessage()
        }, 2000);
        return () => clearInterval(interval);
    });

    return (
        <Container>
            <CssBaseline/>
            <Grid container spacing={2} className={classes.root} alignItems="center">
                <InputGroup printValues={printValues} sendMassage={sendMassage}/>
            </Grid>
            <Grid item xs>
                <List component="nav" aria-label="secondary mailbox folders">
                    {item}
                </List>
            </Grid>
        </Container>
    );
}

export default App;
