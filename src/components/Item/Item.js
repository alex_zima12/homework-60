import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import TextField from '@material-ui/core/TextField';

const Item = props => {
    return (
        <ListItem >
            <TextField
                label="Пользователь:"
                defaultValue={props.author}
            />
            <TextField style={{ marginLeft: 30, width: 200}}
                label="Дата:"
                defaultValue={new Date(props.date)}
            />
            <TextField style={{ width: 650, marginLeft: 30}}
                label="Сообщение: "
                defaultValue={props.message}
            />

        </ListItem>

    );
};

export default Item ;