import React from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
const InputGroup = props => {
    return (
        <>
            <Grid item>
            <TextField
                        type='text'
                       name="author"
                       onChange={props.printValues}
                       label="Пользователь"
                       required="required"
                       variant="outlined" />
            </Grid >
                <Grid item xs>
            <TextField
                        type='text'
                       fullWidth
                       name="message"
                       onChange={props.printValues}
                       label="Введите сообщение"
                       required="required"
                       variant="outlined" />
                </Grid >
                    <Grid item>
                        <Button onClick={props.sendMassage} variant="contained"> Отправить </Button>
              </Grid >
        </>
    )
};

export default InputGroup;